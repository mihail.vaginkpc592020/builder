package builder;

import model.Turbine;

public class TurbineBuilder {

    private Turbine turbine = new Turbine();

    public TurbineBuilder isWorking(Boolean isWorking) {
        turbine.setWorking(isWorking);
        return this;
    }

    public Turbine build() {
        return turbine;
    }
}
