package builder;

import model.Compressor;

public class CompressorBuilder {

    private Compressor compressor = new Compressor();


    public CompressorBuilder rotorDrum(String rotorDrum) {
        compressor.setRotorDrum(rotorDrum);
        return this;
    }

    public CompressorBuilder airIntake(String airIntake) {
        compressor.setAirIntake(airIntake);
        return this;
    }

    public CompressorBuilder driveShaft(String driveShaft) {
        compressor.setDriveShaft(driveShaft);
        return this;
    }

    public CompressorBuilder rotorDisc(String rotorDisc) {
        compressor.setRotorDisc(rotorDisc);
        return this;
    }

    public CompressorBuilder statorBlade(String statorBlade) {
        compressor.setStatorBlade(statorBlade);
        return this;
    }

    public Compressor build() {
        return compressor;
    }
}
