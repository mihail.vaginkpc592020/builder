package builder;

import model.Engine;
import model.Fuselage;
import model.Plain;

public class PlainBuilder {
    private Plain plain = new Plain();

    public PlainBuilder() {

    }

    public PlainBuilder engine(Engine engine) {
        plain.setEngine(engine);
        return this;
    }

    public PlainBuilder fuselage(Fuselage fuselage) {
        plain.setFuselage(fuselage);
        return this;
    }

    public Plain build() {
        return plain;
    }

}
