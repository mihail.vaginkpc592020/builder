package builder;

import model.Wing;

public class WingBuilder {

    private Wing wing = new Wing();

    public WingBuilder airBrake(String airBrake) {
        wing.setAirBrake(airBrake);
        return this;
    }

    public WingBuilder centerSection(String centerSection) {
        wing.setCenterSection(centerSection);
        return this;
    }

    public WingBuilder theBow(String theBow) {
        wing.setTheBow(theBow);
        return this;
    }

    public WingBuilder aileron(String aileron) {
        wing.setAileron(aileron);
        return this;
    }

    public Wing build() {
        return wing;
    }
}
