package builder;

import model.Cockpit;

public class CockpitBuilder {

    private Cockpit cockpit = new Cockpit();

    public CockpitBuilder saidStick(String saidStick) {
        cockpit.setSaidStick(saidStick);
        return this;
    }

    public CockpitBuilder window(String window) {
        cockpit.setWindow(window);
        return this;
    }

    public CockpitBuilder dashboard(String dashboard) {
        cockpit.setDashboard(dashboard);
        return this;
    }

    public Cockpit build() {
        return cockpit;
    }
}
