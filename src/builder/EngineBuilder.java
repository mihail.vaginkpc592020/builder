package builder;

import model.Compressor;
import model.Engine;
import model.Turbine;

public class EngineBuilder {
    private Engine engine = new Engine();

    public EngineBuilder compressor(Compressor compressor) {
        engine.setCompressor(compressor);
        return this;
    }

    public EngineBuilder turbine(Turbine turbine) {
        engine.setTurbine(turbine);
        return this;
    }

    public EngineBuilder power(Integer power) {
        engine.setPower(power);
        return this;
    }

    public Engine build() {
        return engine;
    }

}
