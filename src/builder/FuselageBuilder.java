package builder;

import model.Cockpit;
import model.Fuselage;
import model.Wing;

public class FuselageBuilder {

    private Fuselage fuselage = new Fuselage();

    public FuselageBuilder wing(Wing wing) {
        fuselage.setWing(wing);
        return this;
    }

    public FuselageBuilder cockpit(Cockpit cockpit) {
        fuselage.setCockpit(cockpit);
        return this;
    }

    public Fuselage build() {
        return fuselage;
    }
}
