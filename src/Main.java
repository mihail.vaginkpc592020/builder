import builder.*;
import model.Plain;

public class Main {
    public static void main(String[] args) {
        Plain plain = new PlainBuilder()
                .engine(new EngineBuilder()
                        .turbine(new TurbineBuilder().isWorking(true).build())
                        .power(1000)
                        .compressor(new CompressorBuilder()
                                .airIntake("хрень какая-то")
                                .driveShaft("я не помню что это")
                                .rotorDisc("Диск ротора")
                                .rotorDrum("Барабан ротора")
                                .statorBlade("по-русски - это лопата").build())
                        .build())
                .fuselage(new FuselageBuilder()
                        .cockpit(new CockpitBuilder()
                                .saidStick("Штурвал")
                                .dashboard("Панель задач")
                                .window("блин окно классное").build())
                        .wing(new WingBuilder()
                                .aileron("Я не помню")
                                .airBrake("воздушное что-то")
                                .centerSection("центральная секция")
                                .theBow("боф")
                                .build())
                        .build())
                .build();

        System.out.println(plain);
    }
}