package model;

public class Fuselage {
    private Wing wing;
    private Cockpit cockpit;

    public Fuselage() {
    }

    public void setWing(Wing wing) {
        this.wing = wing;
    }

    public void setCockpit(Cockpit cockpit) {
        this.cockpit = cockpit;
    }

    @Override
    public String toString() {
        return "Fuselage{" +
                "wing=" + wing +
                ", cockpit=" + cockpit +
                '}';
    }
}
