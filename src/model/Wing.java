package model;

public class Wing {

    private String airBrake;
    private String centerSection;
    private String theBow;
    private String aileron;

    public void setAirBrake(String airBrake) {
        this.airBrake = airBrake;
    }

    public void setCenterSection(String centerSection) {
        this.centerSection = centerSection;
    }

    public void setTheBow(String theBow) {
        this.theBow = theBow;
    }

    public void setAileron(String aileron) {
        this.aileron = aileron;
    }

    @Override
    public String toString() {
        return "Wing{" +
                "airBrake='" + airBrake + '\'' +
                ", centerSection='" + centerSection + '\'' +
                ", theBow='" + theBow + '\'' +
                ", aileron='" + aileron + '\'' +
                '}';
    }
}
