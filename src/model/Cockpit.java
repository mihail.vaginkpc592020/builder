package model;

public class Cockpit {

    private String saidStick;
    private String window;
    private String dashboard;

    public void setSaidStick(String saidStick) {
        this.saidStick = saidStick;
    }

    public void setWindow(String window) {
        this.window = window;
    }

    public void setDashboard(String dashboard) {
        this.dashboard = dashboard;
    }

    @Override
    public String toString() {
        return "Cockpit{" +
                "saidStick='" + saidStick + '\'' +
                ", window='" + window + '\'' +
                ", dashboard='" + dashboard + '\'' +
                '}';
    }
}
