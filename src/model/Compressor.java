package model;

public class Compressor {

    private String rotorDrum;
    private String airIntake;
    private String driveShaft;
    private String rotorDisc;
    private String statorBlade;

    public void setRotorDrum(String rotorDrum) {
        this.rotorDrum = rotorDrum;
    }

    public void setAirIntake(String airIntake) {
        this.airIntake = airIntake;
    }

    public void setDriveShaft(String driveShaft) {
        this.driveShaft = driveShaft;
    }

    public void setRotorDisc(String rotorDisc) {
        this.rotorDisc = rotorDisc;
    }

    public void setStatorBlade(String statorBlade) {
        this.statorBlade = statorBlade;
    }

    @Override
    public String toString() {
        return "Compressor{" +
                "rotorDrum='" + rotorDrum + '\'' +
                ", airIntake='" + airIntake + '\'' +
                ", driveShaft='" + driveShaft + '\'' +
                ", rotorDisc='" + rotorDisc + '\'' +
                ", statorBlade='" + statorBlade + '\'' +
                '}';
    }
}
