package model;


public class Plain {

    private Engine engine;
    private Fuselage fuselage;

    public void setEngine(Engine engine) {
        this.engine = engine;
    }

    public void setFuselage(Fuselage fuselage) {
        this.fuselage = fuselage;
    }

    @Override
    public String toString() {
        return "Plain{" +
                "engine=" + engine +
                ", fuselage=" + fuselage +
                '}';
    }
}
