package model;

public class Engine {

    private Compressor compressor;
    private Turbine turbine;
    private Integer power;

    public void setPower(Integer power) {
        this.power = power;
    }

    public void setTurbine(Turbine turbine) {
        this.turbine = turbine;
    }

    public void setCompressor(Compressor compressor) {
        this.compressor = compressor;
    }

    @Override
    public String toString() {
        return "Engine{" +
                "compressor=" + compressor +
                ", turbine=" + turbine +
                ", power=" + power +
                '}';
    }
}
