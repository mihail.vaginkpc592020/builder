package model;

public class Turbine {

    private Boolean isWorking;

    public void setWorking(Boolean working) {
        isWorking = working;
    }

    @Override
    public String toString() {
        return "Turbine{" +
                "isWorking=" + isWorking +
                '}';
    }
}
